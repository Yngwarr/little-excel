# Little Excel

Тестовое задание "[Маленький Эксель](https://docs.xored.com/pages/viewpage.action?pageId=26378756)".

Написано на языке [Clojure](https://clojure.org/), собрано с помощью [leiningen](https://leiningen.org/).

# Компиляция и запуск

```
lein uberjar
java -jar ./target/uberjar/excel-1.0.0-standalone.jar < input_file.txt
```

# Примеры входных данных

В директории `samples` находятся примеры входных файлов:

* `just.txt`, `no-ref.txt`, `from-docs.txt`, `fibonacci.txt` — корректные таблицы,
  ячейки не должны содержать сообщений об ошибках.
* `badfmt.txt` — обработка некорректных формул. Ожидаемый вывод:

```
2	#BADFMT	#BADFMT
3	#BADFMT	#VALUE
```

* `badref.txt` — обработка некорректных ссылок в формулах (например, на строку
  или за пределы таблицы). Ожидаемый вывод:

```
2	#BADREF	just
3	#BOUNDS	code
```

* `cycle.txt`, `lil-cycle.txt` — обработка циклических зависимостей формул.
  Вместо некорректной формулы выводится сообщение `#CYCLE`.

* `div-by-zero.txt` — обработка деления на ноль в формуле. Ожидаемый вывод:

```
#DIVBY0	0
#DIVBY0	#DIVBY0
```

* `head-mismatch.txt`, `irregular-size.txt` — обработка несоответствия объявленного
  в заголовке размера таблицы действительному. В таком случае лишние ячейки
  отбрасываются, а недостающие создаются пустыми.

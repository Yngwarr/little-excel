(ns excel.core
  (:require [clojure.string :as str] [clojure.set :as set])
  (:gen-class))

; print line to stderr
(defn perror [msg]
  (let [*out* *err*] (println msg)))

; parsing numbers with no exceptions
(defn str->int [n]
  (try (Integer. n)
    (catch IllegalArgumentException e nil)))

(defn str->float [n]
  (try (Float. n)
    (catch IllegalArgumentException e nil)))

; lazy sequence with infinite number of the same value
(defn list-of [x]
  (lazy-cat [x] (list-of x)))

; concatenates a sequence with an infinite number of a particular value
; e.g.: (take 10 (fill [1 2 3] 0)) -> (1 2 3 0 0 0 0 0 0 0)
(defn fill [xs x]
  (lazy-cat xs (list-of x)))

; length of the English alphabet
(def max-rows 26)
; number of digits (excluding zero, since the first cell is A1)
(def max-cols 9)

; represents a node of AST
(defn node
  ([kind] (vector kind))
  ([kind value] (vector kind value)))

(defn node-val [x]
  (second x))

; enough to divide nodes from ints and strings
(defn node? [x]
  (vector? x))

(defn node-kind? [kind x]
  (= (first x) kind))

(defn err-msg [txt]
  (format "#%s" txt))

; creates an error node with a given message
; e.g.: ["PANIC"] -> [:err "#PANIC"]
(defn err [msg]
  (node :err (err-msg msg)))

; makes a cell name from its coordinates (started at 0)
; e.g.: [0 0] -> "A1", [4 5] -> "F5"
(defn cell-name [row col]
  (letfn [(alph [n] (char (+ (int \A) n)))]
    (format "%s%s" (alph col) (inc row))))

; returns coordinates of a cell with given name
; e.g.: "A1" -> [0 0], "F5" -> [4 5]
(defn cell-index [cname]
  (defn char->int [ch base]
    (- (int ch) (int base)))
  [(char->int (last cname) \1) (char->int (first cname) \A)])

; compares cell positions, required for sorting 
; e.g.: A1 < A2, A2 < B1
(defn cmp-cell-ids [a b]
  (let [[a-row a-col] (cell-index a)
        [b-row b-col] (cell-index b)]
    (if (= a-row b-row)
      (< a-col b-col)
      (< a-row b-row))))

; generates a list of all cell names for a table of size given
; e.g.: [3 2] -> ["A1" "A2" "B1" "B2" "C1" "C2"]
(defn gen-names [rows cols]
  (loop [n 0
         names (transient [])]
    (if (< n rows)
      (recur (inc n) (conj! names (map #(cell-name n %) (range cols))))
      (flatten (persistent! names)))))

; turns an expression into a corresponding node
; e.g.: "3" -> [:num 3], "'hey" -> [:str "hey"]
(defn parse-expr [expr]
  (let [to-int (str->int expr)
        to-float (str->float expr)]
    (cond
      (str/blank? expr) (node :null)
      (not= to-int nil)
        (if (< to-int 0)
          (err "NUM<0")
          (node :num to-int))
      (str/starts-with? expr "'") (node :str (subs expr 1))
      (not= to-float nil) (err "FLOAT")
      :else (err "VALUE"))))

; turns a formula into a list of tokens
; e.g.: "1+B2-3" -> ["1" "+" "B2" "-" "3"]
(defn tokenize [line]
  (str/split line #"(?<=[+\-\*/])|(?=[+\-\*/])"))

; parses a formula without leading '='
; e.g.: "=A1+2" -> [:formula [[:ref "A1"] [:op "+"] [:num 2]]]
(defn parse-formula [f]
  (let [
    ast (loop [ts (tokenize f) res (transient [])]
      (let [head (first ts) tail (rest ts)]
        (if (empty? ts)
          (persistent! res)
          (recur tail (conj! res
            (cond
              (re-matches #"[A-Za-z][0-9]" head) (node :ref (.toUpperCase head))
              (re-matches #"[+\-\*/]" head) (node :op head)
              :else (parse-expr head)))))))]
    (node :formula ast)))

; turns a cell value into a node
(defn parse-cell [cell]
  (if (str/starts-with? cell "=")
    (parse-formula (subs cell 1))
    (parse-expr cell)))

; parses the first line of an input, returns a couple of numbers representing
; table size or an error tuple
; e.g.: "2\t3" -> [2 3], "two\tthree" -> [:err :not-a-number]
(defn parse-header [line]
  (let [size (->> (str/split line #"\t") (map str->int) vec)]
    (if (not= (count size) 2)
      [:err :length-mismatch]
      (let [[rows cols] size]
        (cond
          (or (= rows nil) (= cols nil)) [:err :not-a-number]
          (or (< rows 0) (< cols 0)) [:err :negative]
          (> rows max-rows) [:err :row-bounds]
          (> cols max-cols) [:err :col-bounds]
          :else size)))))

; parses the whole table from a list of row lines into a map of cells
; e.g.: "1\t2
;        3\t4" [2 2] -> {"A1" 1 "A2" 2 "B1" 3 "B2" 4}
(defn parse-table [lines [rows cols]]
  (let [table (take rows (map #(take cols (fill (str/split % #"\t") "")) lines))
        ids (gen-names rows cols)]
    (apply conj (map #(hash-map %1 (parse-cell %2)) ids (flatten table)))))

(defn eval-formula [cid table]
  ; ops is a map of available operations
  (let [ops {"+" + "-" - "*" + "/" quot}
        f (node-val (get table cid))
        ; replaces a value in a map
        add (fn [value tab] (conj tab {cid value}))]
    (loop [head (first f)
           tail (rest f)
           ; accumulator value
           acc nil
           ; current operation
           op nil
           tab table]
      (cond
        (and (nil? head) (empty? tail)) (add acc tab)
        ; error evaluates into a string
        (node-kind? :err head) (add (node-val head) tab)
        ; string references are prohibited
        (node-kind? :str head) (add (err-msg "STR") tab)
        (node-kind? :op head)
          ; trailing, leading and sequential operations are considered errors
          ; e.g.: "3-2+", "+3-2", "3+-2"
          (if (or (empty? tail) (some? op))
            (add (err-msg "BADFMT") tab)
            (recur (first tail) (rest tail) acc (node-val head) tab))
        (node-kind? :num head)
          (cond
            (nil? acc) (recur (first tail) (rest tail) (node-val head) op tab)
            (and (= op "/") (= (node-val head) 0)) (add (err-msg "DIVBY0") tab)
            :else (recur (first tail) (rest tail) ((get ops op) acc (node-val head)) nil tab))
        (node-kind? :ref head)
          (let [value (get tab (node-val head))]
            (cond
              ; value is not presented in table
              (nil? value) (add (err-msg "BOUNDS") tab)
              ; errors and strings
              (string? value) (add (err-msg "BADREF") tab)
              :else (recur (node :num value) tail acc op tab)))))))

(defn eval-cell [cid table]
  (let [cell (get table cid)]
    (cond
      (not (node? cell)) table
      (node-kind? :formula cell) (eval-formula cid table)
      (node-kind? :null cell) (conj table {cid ""})
      ; node is :num or :str
      :else (conj table {cid (node-val cell)}))))

; constructs a hash-table of dependencies
(defn find-refs [table]
  (into {}
    (map
      (fn [[k v]]
        [k (if (node-kind? :formula v)
             (->> (second v) (filter #(node-kind? :ref %)) (map node-val))
             '())]) table)))

; determines if cell has any cyclic dependencies using DFS
; an algorithm is described here: http://bit.ly/2Wo9Hyt
(defn cycle? [v deps]
  (loop [stack [v] grey #{v} black #{}]
    (if (empty? stack)
      false
      (let [cur (peek stack) ds (remove black (get deps cur)) nxt (first ds)]
        (cond
          (some grey ds) true
          (empty? ds) (recur (pop stack) (disj grey cur) (conj black cur))
          :else (recur (conj stack nxt) (conj grey nxt) black))))))

; replaces all the cyclic cells with an error message
(defn rm-cycles [deps]
  (into {}
    (map
      (fn [[k v]]
        [k (if (cycle? k deps) [:err (err-msg "CYCLE")] v)]) deps)))

; evaluates all the cells in a table (presented as a hash-map)
(defn evaluate [table]
  ; dependency lists are needed to determine the right evaluation order
  (let [deps (-> table find-refs rm-cycles)]
    ; ids is the list of cells not yet evaluated
    (loop [ids (keys table) tab table]
      (let [k (first ids)
            ds (get deps k)]
        (cond
          (empty? ids) tab
          ; there may be an error with dependencies 
          (= (first ds) :err)
            (recur (rest ids) (conj tab {k (second ds)}))
          (or (empty? ds) (empty? (set/intersection (set ds) (set ids))))
            (recur (rest ids) (eval-cell k tab))
          ; if cell has any unresolved deps, just move it to the end of the list
          :else
            (recur (concat (rest ids) (list k)) tab))))))

; prints the evaluation result to *out* as a table
(defn print-table [table [rows cols]]
  (loop [row 0
         ; table has to be sorted beforehand since the output order matters
         values (->> table (into (sorted-map-by cmp-cell-ids)) vals)]
    (when (not= row rows)
      (println (str/join "\t" (take cols values)))
      (recur (inc row) (drop cols values)))))

; takes all the input file content as a string, prints the result as a table
(defn process [content]
  (let [lines (str/split-lines content)
        size (parse-header (first lines))]
    (when (= (first size) :err)
      (case (second size)
        (:length-mismatch :not-a-number)
          (perror (str "Error: first line must contain two non-negative"
                       " numbers divided by a tab character."))
        :negative
          (perror "Error: size cannot be a negative number.")
        :row-bounds
          (perror (format "Error: rows number must not exceed %s." max-rows))
        :col-bounds
          (perror (format "Error: cols number must not exceed %s." max-cols))
        (perror "Unknown error while parsing header."))
      (System/exit 1))
    (print-table (evaluate (parse-table (rest lines) size)) size)))

(defn -main [& args]
  (process (slurp *in*)))

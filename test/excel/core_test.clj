(ns excel.core-test
  (:require [clojure.test :refer :all]
            [excel.core :refer :all]))

(deftest parsing
  (testing "Valid case"
    (is (=
         (parse-table ["1" "=A1+3" "" "'just code"] [2 2])
         {"A1" [:num 1] "A2" [:formula [[:ref "A1"] [:op "+"] [:num 3]]]
          "B1" [:null] "B2" [:str "just code"]})))
  ; TODO test headers
  )
